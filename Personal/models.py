from django.db import models

# Create your models here.
class Tipo_ubicacion(models.Model):
    nombre=models.CharField(max_length=35)
    estado=models.BooleanField(default=True)

    def __str__(self):
        return self.nombre

class Area(Tipo_ubicacion):
    pass
    
    def __str__(self):
        return self.nombre

class Turno(Tipo_ubicacion):
    pass

    def __str__(self):
        return self.nombre

class Empleado(models.Model):
    nombre=models.CharField(max_length=200)
    apellido=models.CharField(max_length=200)
    fotografia=models.ImageField(upload_to='photos')
    estado=models.BooleanField(default=True)
    area=models.ForeignKey(Area, on_delete=models.CASCADE)
    turno=models.ManyToManyField(Turno)


    def __str__(self):
        return self.nombre

    def get_turnos(self):
        return "\n".join([t.nombre for t in self.turno.all()])
    get_turnos.short_description = 'Turnos'

 
