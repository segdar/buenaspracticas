from django.contrib import admin

from .models import *
# Register your models here.

class EmpleadoAdmin(admin.ModelAdmin):
    list_display = ('nombre','apellido','fotografia','get_turnos','estado')


admin.site.register(Area)
admin.site.register(Turno)

admin.site.register(Empleado,EmpleadoAdmin)
