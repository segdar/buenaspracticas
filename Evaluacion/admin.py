from django.contrib import admin

from .models import *

# Register your models here.

class EvaluacionAdmin(admin.ModelAdmin):
    list_display=['fecha','detalle_evaluacion']

class DetalleAdmin(admin.ModelAdmin):
    list_display=('get_detalle','get_items_evaluar','accion_correctiva')

class CatalogoAdmin(admin.ModelAdmin):
    list_deisplay=('nombre','categoria','valor')

admin.site.register(Categoria_items)
admin.site.register(Catalogo_Items,CatalogoAdmin)
admin.site.register(Detalle_evaluacion,DetalleAdmin)
admin.site.register(Evaluacion,EvaluacionAdmin)
admin.site.register(Accion_correctiva)