from .models import *
from django import forms

class Evaluacion_accioncorrectivaForm(forms.ModelForm):
    class Meta:
        model=Accion_correctiva
        fields='__all__'
        
class CategorioForm(forms.ModelForm):
    class Meta:
        model=Categoria_items
        fields='__all__'

class CatalogoForm(forms.ModelForm):
    class Meta:
        model=Catalogo_Items
        fields='__all__'

class Detalle_evaluacionForm(forms.ModelForm):
    class Meta:
        model=Detalle_evaluacion
        fields='__all__'

class EvaluacionForm(forms.ModelForm):
    class Meta:
        model=Evaluacion
        fields='__all__'
