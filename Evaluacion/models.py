from django.db import models
from Personal.models import Empleado,Area
# Create your models here.

class Accion_correctiva(models.Model):
    nombre=models.CharField(max_length=300)
    estado=models.BooleanField(default=True, blank=True)
#gotogeter
    def __str__(self):
        return self.nombre
    
class Categoria_items(models.Model):
    nombre=models.CharField(max_length=200)
    estado=models.BooleanField(default=True)
    
    def __str__(self):
        return self.nombre

class Catalogo_Items(models.Model):
    categoria=models.ForeignKey(Categoria_items,on_delete=models.CASCADE)
    nombe=models.CharField(max_length=200)
    valor=models.IntegerField()
    estado=models.BooleanField(default=True)


    def __str__(self):
        return self.nombe


class Detalle_evaluacion(models.Model):
    persona=models.ManyToManyField(Empleado)
    item_evaluar=models.ManyToManyField(Categoria_items)
    recomendacion=models.CharField(max_length=300)
    accion_correctiva=models.ForeignKey(Accion_correctiva,on_delete=models.CASCADE)

    def __str__(self):
        return self.recomendacion

    def get_detalle(self):
        return '\n'.join([p.nombre for p in self.Empleado.all()])
    get_detalle.short_description='Personas'

    def get_items_evaluar(self):
        return '\n'.join([i.nombre for i in self.Categoria_items.all()])
    get_items_evaluar.short_description="Itemas Evaluados"

class Evaluacion(models.Model):
    fecha=models.DateField()
    detalle_evaluacion=models.ForeignKey(Detalle_evaluacion, on_delete=models.CASCADE)

    def __str__(self):
        return '{}'.format(self.detalle_evaluacion)
    
    