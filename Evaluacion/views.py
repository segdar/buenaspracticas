from django.shortcuts import render,redirect
from .models import *
from .forms import *
from django.db.models import Q
from django.contrib.auth.decorators import login_required
# Create your views here.

@login_required
def registro_accion(request):
    accion=Evaluacion_accioncorrectivaForm()
    if request.method=='POST':
        form=Evaluacion_accioncorrectivaForm(request.POST)
        if form.is_valid():
            instance = form.save()
        return redirect ('vista_acciones')
    return render(request,'acciones_correctivas.html',{'form':accion})

@login_required
def categoria(request):
    categoria=CategorioForm()
    if request.method=='POST':
        form=CategorioForm(request.POST)
        if form.is_valid():
            instance = form.save()
        return redirect ('listar_categoria')
    return render(request,'categoria.html',{'form':categoria})


@login_required
def catalogo(request):
    categorias=Categoria_items.objects.all()
    if request.method=='POST':
        form=CatalogoForm(request.POST)
        if form.is_valid():
            instance = form.save()
          #  return redirect ('')
    return render(request,'catalogo.html',{'forms':categorias})


@login_required
def detalle_eva(request):
    detalle=Detalle_evaluacionForm
    if request.method=='POST':
        form=Detalle_evaluacionForm(request.POST)
        if form.is_valid():
            instance = form.save()
          #  return redirect ('')
    return render(request,'detalle.html',{'form':detalle})


@login_required
def evaluacion(request):
    detalles=Detalle_evaluacion.objects.all()
    if request.method=='POST':
        form=EvaluacionForm(request.POST)
        if form.is_valid():
            instance = form.save()
          #  return redirect ('')
    return render(request,'evaluacion.html',{'form':detalles})


#listar registros
@login_required
def acciones_corretivas_listar(request):
    acciones=Accion_correctiva.objects.all()
    return render(request, 'acciones_listar.html',{'acciones':acciones})

@login_required
def categoria_listar(request):
    categoria=Categoria_items.objects.all()
    return render(request, 'categoria_listar.html',{'categorias':categoria})



#editar registros
@login_required
def acciones_correctivas_editar(request,id_acciones):
    acciones=Accion_correctiva.objects.get(id=id_acciones)
    if request.method=='GET':
        form=Evaluacion_accioncorrectivaForm(instance=acciones)
    else:
        form=Evaluacion_accioncorrectivaForm (request.POST, instance=acciones)
        if form.is_valid():
            form.save()
        return redirect('vista_acciones')
    return render(request,'acciones_correctivas.html',{'form':acciones})

@login_required
def categoria_editar(request,id_catalogo):
    categoria=Categoria_items.objects.get(id=id_catalogo)
    if request.method=='GET':
        form=CategorioForm(instance=categoria)
    else:
        form=CategorioForm (request.POST, instance=categoria)
        if form.is_valid():
            form.save()
        return redirect('listar_categoria')
    return render(request,'categoria.html',{'form':categoria})

    
#Eliminar
def acciones_eliminar(request,id_acciones):
    accion=Accion_correctiva.objects.get(id=id_acciones)
    if request.method=='POST':
        accion.delete()
        return redirect('vista_acciones')
    return render(request,'acciones_eliminar.html',{'form':accion})

def categoria_eliminar(request,id_categoria):
    categoria=Categoria_items.objects.get(id=id_acciones)
    if request.method=='POST':
        categoria.delete()
        return redirect('listar_categoria')
    return render(request,'categoria_eliminar.html',{'form':categoria})