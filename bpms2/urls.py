"""bpms URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url
from django.urls import path,include
from .views import * 
from Evaluacion.views import *


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', home, name='home'),
    path('login',login, name='login'),
    #acciones
    path('registro_accion', registro_accion, name='acciones'),
    path('acciones_listar', acciones_corretivas_listar, name='vista_acciones'),
    path('editar/<int:id_acciones>/', acciones_correctivas_editar, name='editar_acciones'),
    path('eliminar/<int:id_acciones>/', acciones_eliminar, name='eliminar_acciones'),

    #categoria
    path('registro_categoria', categoria, name='registro_categoria'),
    path('listar_categoria', categoria_listar, name='listar_categoria'),
    path('editar/<int:id_categoria>/', categoria_editar, name='editar_categoria'),
    path('eliminar/<int:id_acciones>/', categoria_eliminar, name='eliminar_categoria'),

    
    #catalogo
    path('registro_catalogo', catalogo, name='registro_catalogo'),
    path('registro_evaluacion', evaluacion, name='registro_evaluacion'),

   
    path('logout', logout, name='logout'),
   
]
